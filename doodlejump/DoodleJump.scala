package doodlejump
import s1._
import scala.io.Source
import java.io.File
import java.io.PrintWriter

object DoodleJump extends App {
  
  val height = 800
  val width = 450
  
  val playerStartHeight = 700
  val cameraMovePoint = 2 * height / 3
  
  val background = rectangle(width,height,LightGray)
  
  val rndm = new scala.util.Random(System.currentTimeMillis)
  
  var state = 0 //0 = game, 1 = game over
  
  val doodlePic = circle(17, Yellow)
  val doodlePic2 = rectangle(20,25, Green)
  val doodlePic3 = circle(6, Black)
  val doodlePic4 = rectangle(15, 8, Yellow)
  val doodlePic5 = rectangle(5, 10, Black)
  val doodlePic6 = rectangle(5, 10, Black)

  val betterDoodle = 
      doodlePic.onto(
          doodlePic2,
          my = BottomCenter,
          atIts = TopCenter)
          
  val betterDoodle2 =
    doodlePic4.onto(
        betterDoodle,
        my = TopLeft,
        atIts = TopCenter)
        
  val betterDoodle3 =
    doodlePic3.onto(
        betterDoodle2,
        my = TopCenter,
        atIts = TopCenter)
        
  val betterDoodle4 =
    doodlePic5.onto(
        betterDoodle3,
        my = TopCenter,
        atIts = BottomCenter)
        
  val betterDoodle5 =
    doodlePic5.onto(
        betterDoodle4,
        my = BottomLeft,
        atIts = BottomLeft)
        
  
  object Char {
    var x = width/2
    var xSpeed = 5
    
    var dir = 0 //1 oikee -1 vasen 0 ei mihinkään
    
    var y = playerStartHeight
    var ySpeed = 0.0
    val yAccel = 0.5

    val charPic = betterDoodle5
    val charWidth = this.charPic.width/2
    
    def move() = {
      if (state == 0) {
        y = y - ySpeed.toInt
        ySpeed = ySpeed - yAccel
        x = (x + dir * xSpeed + width) % width
      }
    }
    
    def pic(pic: Pic) = {
      var picture = pic.place(this.charPic,new Pos(this.x,this.y))
      //tarkistaa meneekö hahmon kuva ruudun yli
      if (x - charWidth < 0) {
        //näyttää vasemmalta yli menneen osan oikealla
        picture = picture.place(this.charPic, new Pos(this.x + width,this.y))
      } else if (x + charWidth > 450) {
        //näyttää oikealta yli menneen osan vasemmalla
        picture = picture.place(this.charPic, new Pos(this.x - width,this.y))
      }
      picture
    }
    
    def jump() = {
      this.ySpeed = 15
    }
    
    def reset = {
      y = playerStartHeight
      ySpeed = 0
      x = width / 2
      dir = 0
    }
    
  }
  
  val trampolineH = 10
  val trampolineW = 40
  
  var points = 0
  val save = new File("highscore.txt")  // File in which to keep high score
  var highScore = 0
  // Read high score from file if it exists
  if (save.exists) Source.fromFile("highscore.txt").foreach(score => highScore = score.asDigit)
  
  class Trampoline(var y: Int, val first: Boolean = false) {
    
    var x = rndm.nextInt(width-trampolineW*2) + trampolineW
    var pic = rectangle(trampolineW*2,trampolineH*2,Black)
    var pointGiven = false
    
    if (first) {
      x = width / 2
      pointGiven = true
      pic = rectangle(width,trampolineH*2,Black)
    }
    
    val initialy = y
    
    def move(distance: Int) = y += distance
    
    def tick() = {
      
      if (this.y > 800) {
        this.newPos()
      }
      val bound = Bounds(this.x - pic.width / 2 - Char.charWidth, this.y - pic.height / 2 - Char.charWidth, pic.width + Char.charWidth*2, pic.height)
      if (bound.contains(Char.x, Char.y) && Char.ySpeed < 0) {
        Char.jump()
        if (!this.pointGiven) {
          points += 1
          this.pointGiven = true
        }
      }

    }

    def newPos() = {
      this.y = 0
      this.x = rndm.nextInt(width-trampolineW*2) + 40
      this.pointGiven = false
      if (first) pic = rectangle(trampolineW*2,trampolineH*2,Black)
    }
    
    def reset = {
      y = initialy
      if (first) {
        x = width / 2
        pointGiven = true
        pic = rectangle(width,trampolineH*2,Black)
      }
    }
    
  }
  
   val obstacleCircle = circle(20, Red)
    val obstacleRectOnTop = rectangle(15, 5, Yellow)
    val obstacleRectOnSide = rectangle(5, 15, Yellow)
    
    val betterObstacle = 
      obstacleRectOnTop.onto(
          obstacleCircle,
          my = BottomCenter,
          atIts = TopCenter)
          
  val betterObstacle2 =
    obstacleRectOnTop.onto(
        betterObstacle,
        my = TopCenter,
        atIts = BottomCenter)
        
  val betterObstacle3 =
    obstacleRectOnSide.onto(
        betterObstacle2,
        my = CenterRight,
        atIts = CenterLeft)
        
  val betterObstacle4 =
    obstacleRectOnSide.onto(
        betterObstacle3,
        my = CenterLeft,
        atIts = CenterRight)

        
  //obstacle that ends the game if the player touches it
 class Alien (var y: Int) {
    
    val pic = betterObstacle4
    var x = rndm.nextInt(width - this.pic.width/2) + this.pic.width/2
    
    var count = 0
    var speed = 5

    def move() = {
      if (state == 0) y += speed
      if (this.y > 810) {
        this.newPos()
      }
    }

    def newPos() = {
      this.y = 0
      this.x = rndm.nextInt(width - this.pic.width/2) + this.pic.width/2
      count += 1
      if (count % 3 == 0) {
        speed += 1
        count = 0
      }
    }
    
    def reset = {
      this.newPos()
      count = 0
      speed = 5
    }
    
    def gameOver =  Bounds(this.x - this.pic.width/2 - 11, this.y - this.pic.width/2, pic.width + Char.charPic.width*2, this.pic.height).contains(Char.x, Char.y)

  }
  
  val alien = new Alien (0)

  val trampArray = Array(new Trampoline(790, true), new Trampoline(590), new Trampoline(390), new Trampoline(190))
  
  val view = new s1.gui.mutable.View(Char, 60) {
    
    override def onTick() = {
      model.move()
      alien.move()
      trampArray.foreach(_.tick)
      if (model.y < cameraMovePoint) {
        val toMove = cameraMovePoint - model.y
        model.y = cameraMovePoint
        trampArray.foreach(_.move(toMove))
        alien.y += toMove
      }
      if (model.y > height + model.charWidth || alien.gameOver) {
        if (state == 0) println(if (points == 1)  f"You got $points point." else f"You got $points points.")
        if (points > highScore) {
            highScore = points
            var writer = new PrintWriter(save)  // Write high score to a file
            writer.write(highScore.toString)
            writer.close
        }
        points = 0
        state = 1
      }
    }
    
    def makePic() = {
      var pic = background
      for (tramp <- trampArray) {
        pic = pic.place(tramp.pic, new Pos(tramp.x, tramp.y))
      }
      pic = pic.place(alien.pic, new Pos(alien.x, alien.y))
      model.pic(pic)
    }
    
    val dirMap = Map(Key.A -> -1, Key.D -> 1)
    
    override def onKeyDown(key: Key) = {
      if (state == 0) dirMap.get(key).foreach( model.dir = _ )
      if (state == 1) {
        trampArray.foreach(_.reset)
        model.reset
        alien.reset
        state = 0
      }
    }
    
    override def onKeyUp(key: Key) = {
      //tarkistaa vastaako näppäin nykyistä suuntaa
      if (state == 0 && dirMap.get(key).exists( _ == model.dir )) model.dir = 0
    }
  }
  
  view.start()
}